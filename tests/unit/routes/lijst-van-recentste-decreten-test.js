import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | lijst van recentste decreten', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:lijst-van-recentste-decreten');
    assert.ok(route);
  });
});
