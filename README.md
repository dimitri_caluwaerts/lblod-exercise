# lblod-exercise

## Exercise

Build a small web application listing the title and publication date of the 5 latest decrees published by the Flemish Government.

At the time of writing this list contains:

* 2020-12-03 - "houdende wijziging van de Vlaamse Codex Fiscaliteit van 13 december 2013, wat betreft de aangifte van automatische ontspanningstoestellen"
* 2020-12-02 - "tot toekenning van een eenmalige toeslag in het kader van het gezinsbeleid naar aanleiding van de coronacrisis"
* 2020-11-27 - "houdende diverse bepalingen over het gemeenschappelijk vervoer, het algemeen mobiliteitsbeleid, de weginfrastructuur en het wegenbeleid, en de waterinfrastructuur en het waterbeleid"
* 2020-11-25 - "tot wijziging van het Energiedecreet van 8 mei 2009"
* 2020-11-16 - "houdende de instemming met het samenwerkingsakkoord van 18 februari 2020 tot wijziging van het samenwerkingsakkoord van 12 december 2005 tussen de Federale Staat, de Vlaamse Gemeenschap, de Franse Gemeenschap, de Duitstalige Gemeenschap en de Gemeenschappelijke Gemeenschapscommissie inzake de tenuitvoerlegging van de wet van 24 april 2003 tot hervorming van de adoptie"

The data is available via the SPARQL endpoint of the Vlaamse Codex and is structured as documented on https://data.vlaanderen.be/doc/applicatieprofiel/vlaamse-codex/ (tip: hover over the entity and property names to get the corresponding URIs). The endpoint is available on https://codex.vlaanderen.be/sparql.

The decrees can be found by querying legal resources (Rechtsbron) of type https://data.vlaanderen.be/id/concept/AardWetgeving/Decreet. Via the relation 'is gerealiseerd door' (is realized by) you find the corresponding legal expressions (Verschijningsvorm) which contain the properties we're interested in.

The frontend should be built using Ember.js. The query can be executed using fetch as follows:

```javascript
const endpoint = `https://codex.vlaanderen.be/sparql?query=${encodedQuery}`;
const response = await fetch(endpoint, { headers: { 'Accept': 'application/sparql-results+json' } } );
const decisions = await response.json();
```

Some useful pointers if you're new to any of the technologies:

* Getting started with Ember: https://guides.emberjs.com/release/getting-started/quick-start/
* Introduction to SPARQL: https://www.cambridgesemantics.com/blog/semantic-university/learn-sparql/sparql-nuts-bolts/
* Full SPARQL specification: https://www.w3.org/TR/sparql11-query/

We expect you to shortly present your solution. We're not only interested in the final web app, but also in the path towards it (how did you tackle the problem, what resources did you use, how much time did you spend, etc.). You can publish your source code on a platform of your choice.

## Installation

* `git clone <repository-url>` this repository
* `cd lblod-exercise`
* `npm install`

## Running / Development

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).

## Exercise location

* Visit the solution of the exercise at  [http://localhost:4200/lijst-van-recentste-decreten](http://localhost:4200/lijst-van-recentste-decreten)
