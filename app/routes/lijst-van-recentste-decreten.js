import Route from '@ember/routing/route';

function parseHtmlEntities(str) {
  return str.replace(/&#([0-9]{1,3});/gi, function(match, numStr) {
    var num = parseInt(numStr, 10); // read num as normal number
    return String.fromCharCode(num);
  });
}

export default class LijstVanRecentsteDecretenRoute extends Route {
  async model() {
    const query =
      "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
      "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
      "PREFIX eli: <http://data.europa.eu/eli/ontology#> " +
      "SELECT ?title ?datePublication " +
      "WHERE { " +
      "?decreet eli:type_document <https://data.vlaanderen.be/id/concept/AardWetgeving/Decreet> ; " +
      "eli:is_realized_by ?expression . " +
      "?expression eli:title ?title .  " +
      "?expression eli:date_publication ?datePublication . " +
      "} " +
      "ORDER BY DESC( ?datePublication) LIMIT 5";
    const encodedQuery = encodeURIComponent(query);
    const endpoint = `https://cors-anywhere.herokuapp.com/https://codex.vlaanderen.be/sparql?query=${encodedQuery}`;
    const response = await fetch(endpoint, {
      headers: {
        'Accept': 'application/sparql-results+json',
        'origin': 'localhost:4200'
      }
    });
    const decisions = await response.json();
    const {results} = decisions;
    console.log(results);
    return results.bindings.map(result => {
      const title = parseHtmlEntities(result.title.value);
      const datePublication = result.datePublication.value;

      return { title, datePublication };
    });
  }
}
